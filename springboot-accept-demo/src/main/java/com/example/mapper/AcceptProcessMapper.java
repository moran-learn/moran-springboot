package com.example.mapper;

import com.example.entity.AcceptProcess;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-03 11:37
 */
public interface AcceptProcessMapper {
    void saveAcceptProcess(AcceptProcess acceptProcess);
}
