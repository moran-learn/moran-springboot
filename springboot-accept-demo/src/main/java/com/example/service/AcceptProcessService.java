package com.example.service;

import com.example.entity.AcceptProcess;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-03 12:38
 */
public interface AcceptProcessService {

    void saveAcceptProcess(AcceptProcess acceptProcess);
}
