package com.example.service.impl;

import com.example.entity.AcceptProcess;
import com.example.mapper.AcceptProcessMapper;
import com.example.service.AcceptProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-03 12:38
 */
@Service
public class AcceptProcessServiceImpl implements AcceptProcessService {
    Logger log = LoggerFactory.getLogger(AcceptProcessServiceImpl.class);
    private long count = 0;
    @Autowired
    private AcceptProcessMapper acceptProcessMapper;
    @Override
    public void saveAcceptProcess(AcceptProcess acceptProcess) {
        acceptProcessMapper.saveAcceptProcess(acceptProcess);
        count++;
        log.info("已成功插入" + count + "条数据！");
    }
}
