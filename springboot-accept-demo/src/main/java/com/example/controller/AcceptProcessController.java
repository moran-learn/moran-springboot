package com.example.controller;


import com.example.entity.AcceptProcess;
import com.example.result.Result;
import com.example.service.AcceptProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AcceptProcessController {

    @Autowired
    private AcceptProcessService acceptProcessService;
    @PostMapping(value = "/accept/process", produces = "application/json;charset=utf-8")
    public Result receiveProcess(@RequestBody AcceptProcess acceptProcess) {
        acceptProcessService.saveAcceptProcess(acceptProcess);
        return Result.ok();
    }
}
