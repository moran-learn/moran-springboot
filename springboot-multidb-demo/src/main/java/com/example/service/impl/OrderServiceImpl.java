package com.example.service.impl;

import com.example.entity.AcceptProcess;
import com.example.entity.Order;
import com.example.mapper.first.OrderFirstMapper;
import com.example.mapper.second.OrderSecondMapper;
import com.example.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 16:07
 */
@Service
public class OrderServiceImpl implements OrderService {

    Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);
    @Autowired
    private OrderFirstMapper orderFirstMapper;

    @Autowired
    private OrderSecondMapper orderSecondMapper;

    @Override
    public void saveOrder(Order order) {
        orderFirstMapper.saveOrder(order);
        log.info("数据源First数据插入成功！");
        orderSecondMapper.saveOrder(order);
        log.info("数据源Second数据插入成功！");
    }

    @Override
    public void saveAcceptProcess(AcceptProcess acceptProcess) {
        orderFirstMapper.saveAcceptProcess(acceptProcess);
        log.info("数据源First数据插入成功, {}", acceptProcess);
        orderSecondMapper.saveAcceptProcess(acceptProcess);
        log.info("数据源Second数据插入成功！{}", acceptProcess);
    }
}
