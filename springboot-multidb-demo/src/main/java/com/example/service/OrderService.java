package com.example.service;

import com.example.entity.AcceptProcess;
import com.example.entity.Order;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 16:06
 */
public interface OrderService {
    void saveOrder(Order order);
    void saveAcceptProcess(AcceptProcess acceptProcess);
}
