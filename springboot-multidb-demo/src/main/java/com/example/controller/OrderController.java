package com.example.controller;

import com.example.entity.AcceptProcess;
import com.example.entity.Order;
import com.example.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 16:11
 */
@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;
    @RequestMapping("order/saveOrder")
    public String saveOrder(Order order){
        orderService.saveOrder(order);
        return "success";
    }
    @RequestMapping("order/saveAcceptProcess")
    public String saveAcceptProcess(@RequestBody AcceptProcess acceptProcess){
        orderService.saveAcceptProcess(acceptProcess);
        return "success";
    }
}
