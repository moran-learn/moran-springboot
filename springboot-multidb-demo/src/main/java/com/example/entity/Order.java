package com.example.entity;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 14:20
 */
public class Order {

    private Long id;
    private Long goodsId;
    private Long count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
