package com.example.entity;

import java.io.Serializable;

/**
 * @Description
 * @Author Moran
 * @Date 2023-05-31 22:00
 */
public class AcceptProcess implements Serializable {
    private Long id;
    private String processName;
    private String auditPerson;
    private String auditResult;
    private Integer pushFlag;

    @Override
    public String toString() {
        return "AcceptProcess{" +
                "id=" + id +
                ", processName='" + processName + '\'' +
                ", auditPerson='" + auditPerson + '\'' +
                ", auditResult='" + auditResult + '\'' +
                ", pushFlag=" + pushFlag +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getAuditPerson() {
        return auditPerson;
    }

    public void setAuditPerson(String auditPerson) {
        this.auditPerson = auditPerson;
    }

    public String getAuditResult() {
        return auditResult;
    }

    public void setAuditResult(String auditResult) {
        this.auditResult = auditResult;
    }

    public Integer getPushFlag() {
        return pushFlag;
    }

    public void setPushFlag(Integer pushFlag) {
        this.pushFlag = pushFlag;
    }
}
