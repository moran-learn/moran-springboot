package com.example.mapper.second;

import com.example.entity.AcceptProcess;
import com.example.entity.Order;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 16:01
 */
public interface OrderSecondMapper {
    void saveOrder(Order order);
    void saveAcceptProcess(AcceptProcess acceptProcess);
}
