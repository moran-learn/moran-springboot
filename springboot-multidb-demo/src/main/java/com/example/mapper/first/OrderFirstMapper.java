package com.example.mapper.first;

import com.example.entity.AcceptProcess;
import com.example.entity.Order;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 15:54
 */
public interface OrderFirstMapper {
    void saveOrder(Order order);

    void saveAcceptProcess(AcceptProcess acceptProcess);
}
