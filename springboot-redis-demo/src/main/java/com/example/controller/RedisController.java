package com.example.controller;

import com.example.annotation.LogAnnotation;
import com.example.entity.UserEntity;
import com.example.util.RedisUtil;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-10 11:37
 */
@Slf4j
@RestController
@RequestMapping("redis/")
public class RedisController {

    @Resource
    private RedisUtil redisUtil;

    @RequestMapping("set")
    @LogAnnotation("设置Redis")
    public boolean setRedis(String key) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(Long.valueOf(1));
        userEntity.setGuid(String.valueOf(1));
        userEntity.setName("zhangSan");
        userEntity.setAge(String.valueOf(20));
        userEntity.setCreateTime(new Date());
        return redisUtil.set(key, userEntity);
    }

    @LogAnnotation("获取Redis的值")
    @RequestMapping("get")
    public Object getRedis(String key) {
        return redisUtil.get(key);
    }

    @LogAnnotation("获取Redis的过期时间")
    @RequestMapping("expire")
    public boolean expire(String key) {
        // redis中存储的过期时间60s
        int expireTime = 60;
        return redisUtil.expire(key, expireTime);
    }
}
