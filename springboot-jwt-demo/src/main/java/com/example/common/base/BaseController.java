package com.example.common.base;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * @Description
 * @Author Moran
 * @Date 2023-05-28 17:39
 */
public class BaseController {
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected String UserId;  // 用户id
    protected String authority;  // 用户权限

    @ModelAttribute
    public void parseClaims(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        // 获取到在拦截器里设置的user_claims， 并将其保存到类的成员变量中
        Claims userClaims = (Claims) request.getAttribute("user_claims");
        if (userClaims != null) {
            this.UserId = userClaims.getId();
            this.authority = userClaims.get("authority").toString();
        }
    }
}
