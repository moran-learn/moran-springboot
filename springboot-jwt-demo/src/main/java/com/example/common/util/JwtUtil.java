package com.example.common.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;

/**
 * @Description jwt工具类
 * @Author Moran
 * @Date 2023-05-28 17:29
 */
public class JwtUtil {
    // 设置有效时间
    private static final long period = 7200000;
    private static final String secretString = "12345678901234567890123456789012";
    //私钥
    private static final SecretKey key = Keys.hmacShaKeyFor(secretString.getBytes(StandardCharsets.UTF_8));

    /**
     * 生成token
     */
    public static String generateToken(String userId, String userNick, Map<String, Object> other) {
        //创建jwtBuilder
        JwtBuilder jwtBuilder = Jwts.builder()
                .setClaims(other) // 使用setClaims可以将Map数据进行加密，必须放在其他变量之前
                .setId(userId) //id
                .setSubject(userNick)//主题
                .setExpiration(new Date(System.currentTimeMillis() + period)) // 设置有效期
                .signWith(key);//签名
        return jwtBuilder.compact();
    }

    /**
     * 解析Token
     */
    public static Claims parseToken(String token) {
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();
    }
}
