package com.example.common.config;

import com.example.common.interceptor.TokenInterceptor;
import jakarta.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description 拦截器配置类
 * @Author Moran
 * @Date 2023-05-28 17:20
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Resource
    TokenInterceptor tokenInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //设置所有路径进行拦截
        registry.addInterceptor(tokenInterceptor).addPathPatterns("/**");
    }

}
