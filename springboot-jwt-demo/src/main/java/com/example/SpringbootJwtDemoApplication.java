package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Moran
 */
@SpringBootApplication
public class SpringbootJwtDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJwtDemoApplication.class, args);
    }

}
