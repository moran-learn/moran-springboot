package com.example.controller;

import com.example.annotation.LogAnnotation;
import com.example.common.annotation.Anon;
import com.example.common.base.BaseController;
import com.example.common.util.JwtUtil;
import com.example.exception.BadRequestException;
import com.example.result.ResultVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author Moran
 * @Date 2023-05-28 17:38
 */
@RestController
@RequestMapping("test/")
public class TestController extends BaseController {

    @Anon
    @LogAnnotation("用户登录")
    @RequestMapping(value = "loginUser", produces = "application/json; charset=utf-8")
    public ResultVo loginUser(String name, String password) {
        Map<String, Object> map = new HashMap<>();
        map.put("authority", password);
        return ResultVo.success("success", JwtUtil.generateToken(name, password, map));
    }

    @LogAnnotation("用户注册")
    @RequestMapping(value = "saveUser", produces = "application/json; charset=utf-8")
    public ResultVo saveSysUser(String name, @RequestParam("password") String password) {
        if ("1".equals(name)){
            throw new BadRequestException("注册异常！");
        }
        return ResultVo.success("success", name + password);
    }
}
