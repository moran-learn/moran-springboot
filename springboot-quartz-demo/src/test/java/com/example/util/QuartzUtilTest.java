package com.example.util;

import com.example.entity.QuartzBean;
import org.junit.jupiter.api.Test;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-18 10:31
 */
@SpringBootTest
class QuartzUtilTest {

    @Autowired
    private Scheduler scheduler;

    /**
     * 创建定时任务
     */
    @Test
    void createScheduleJob() {
        QuartzBean quartzBean = new QuartzBean();
        quartzBean.setJobClass("com.example.task.TestTask");
        quartzBean.setJobName("test1");
        quartzBean.setCronExpression("*/10 * * * * ?");
        QuartzUtil.createScheduleJob(scheduler, quartzBean);
    }

    /**
     * 暂停定时任务
     */
    @Test
    void pauseScheduleJob() {
        QuartzUtil.pauseScheduleJob(scheduler, "test");
    }

    @Test
    void resumeScheduleJob() {
        QuartzUtil.resumeScheduleJob(scheduler, "test");
    }

    @Test
    void runOnce() {
        QuartzUtil.runOnce(scheduler, "test1");
    }

    @Test
    void updateScheduleJob() {
        QuartzBean quartzBean = new QuartzBean();
        quartzBean.setJobClass("com.example.task.TestTask");
        quartzBean.setJobName("test");
        quartzBean.setCronExpression("10 * * * * ?");
        QuartzUtil.updateScheduleJob(scheduler, quartzBean);
    }

    @Test
    void deleteScheduleJob() {
        QuartzUtil.deleteScheduleJob(scheduler, "test");
    }
}