package com.example.service.impl;

import com.example.service.QuartzService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-17 19:21
 */
@Service
public class QuartzServiceImpl implements QuartzService {

    @Override
    public void testTask() throws InterruptedException {
        System.out.println("定时任务测试：" + LocalDateTime.now());
        Thread.sleep(5000);
    }
}
