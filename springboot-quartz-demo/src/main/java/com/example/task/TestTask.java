package com.example.task;

import com.example.service.QuartzService;
import jakarta.annotation.Resource;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-17 19:19
 */
public class TestTask extends QuartzJobBean {
    @Resource
    private QuartzService quartzService;
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        try {
            quartzService.testTask();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
