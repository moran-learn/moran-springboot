package com.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author Moran
 * @Date 2023-05-29 20:36
 */
@Controller
@Slf4j
public class LeaveController {

    /**
     * 初始化流程定义
     */
    public static void deployTest() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deployment = repositoryService.createDeployment().addClasspathResource("processes/leaveApplication.bpmn20.xml").name("请假申请流程").deploy();
        log.info("流程部署id: " + deployment.getId());
        log.info("流程部署名称: " + deployment.getName());
    }

    /**
     * 开始流程
     */
    public static void startProcessTest() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();
        Map<String, Object> map = new HashMap<>();
        map.put("assignee0", "Jack");
        map.put("assignee1", "Marry");
        //启动key标识的流程定义，并指定流程定义中的两个参数：assignee0和assignee1
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("leaveApplication", map);
        log.info("流程实例的内容：{}", processInstance);
        log.info("流程部署id：" + processInstance.getId());
        log.info("流程部署名称：" + processInstance.getName());
    }

    /**
     * 查询流转到该所属角色的任务
     */
    public static void findPersonalTaskListTest(String assignee) {
        //对应各流程节点流转下一步人名称，这里第一步从worker开始
        //调用下方completeTask方法可通过审批，再查询下一个名称leader，以此类推直到结束，因为流程图没有不通过的情况所以暂不考虑
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery().processDefinitionKey("leaveApplication").taskAssignee(assignee).list();
        for (Task task : list) {
            System.out.println("流程实例id：" + task.getProcessInstanceId());
            System.out.println("任务id：" + task.getId());
            System.out.println("任务负责人：" + task.getAssignee());
            System.out.println("任务名称：" + task.getName());
        }
    }

    /**
     * 完成任务
     */
    public static void doTaskTest(String assignee) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> tasks = taskService.createTaskQuery().processDefinitionKey("leaveApplication")
                .taskAssignee(assignee)
                .list();
        if (tasks != null && tasks.size() > 0) {
            for (Task task : tasks) {
                log.info("任务名称：{}", task.getName());
                taskService.complete(task.getId());
                log.info("{}，任务已完成", task.getName());

            }
        }
    }

    /**
     * 查询流程
     */
    public static void queryProcessTask(String key) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        ProcessDefinitionQuery definitionQuery = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> definitionList;
        if (key != null) {
            definitionList = definitionQuery
                    .processDefinitionKey(key)
                    .list();
        }

        definitionList = definitionQuery.list();
        //提取所有的流程名称
        List<String> processList = new ArrayList<>();
        for (ProcessDefinition processDefinition : definitionList) {
            log.info("流程名称：{}", processDefinition.getName());
            processList.add(processDefinition.getName());
        }
    }
}
