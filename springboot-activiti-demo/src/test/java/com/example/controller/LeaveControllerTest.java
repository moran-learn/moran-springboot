package com.example.controller;

import org.junit.jupiter.api.Test;

/**
 * @Description
 * @Author Moran
 * @Date 2023-05-29 20:41
 */
class LeaveControllerTest {

    @Test
    void deployTest() {
        LeaveController.deployTest();
    }

    @Test
    void startProcessTest() {
        LeaveController.startProcessTest();
    }


    @Test
    void doTaskTest() {
//        LeaveController.doTaskTest("Jack");
        LeaveController.doTaskTest("Marry");
    }

    @Test
    void queryProcessTask() {
        LeaveController.queryProcessTask("leaveApplication");
    }

    @Test
    void findPersonalTaskListTest() {
        LeaveController.findPersonalTaskListTest("Jack");
        LeaveController.findPersonalTaskListTest("Marry");
    }
}