package com.example.dao;

import com.example.entity.User;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;

import java.util.List;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-10 12:53
 */
@SpringBootTest
class UserDaoTest {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserDao userDao;

    @Test
    public void testFindAll() {
        List<User> list = userDao.findAll();
        list.forEach(user -> logger.info("user={}", user));
    }

    //根据ID查询
    @Test
    public void testFindById() {
        User user = userDao.findById(2L).get();
        logger.info("user={}", user);
    }

    //保存用户
    @Test
    public void testSaveUser() {
        User user = new User();
        user.setDescription("测试");
        user.setUsername("test");
        user.setPassword("test");
        userDao.save(user);
        logger.info("user={}", user);
    }
    //动态查询。根据某个字段查询
    @Test
    public void testFindByExample() {
        User user = new User();
        Example<User> example = Example.of(user);
        user.setUsername("Moran");
        List<User> list = userDao.findAll(example);
        list.forEach(u -> logger.info("user={}", u));
    }

    @Test
    public void testDeleteByPassword() {
        userDao.deleteByPassword("123");
        User user = userDao.findByPassword("moran");
        logger.info("user={}", user);
    }


    @Test
    public void testQueryUserList(){
        List<User> list = userDao.queryUserList();
        list.forEach(u -> logger.info("user = {} ", u));
    }
}