package com.example.dao;

import com.example.entity.User;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-10 12:51
 */
public interface UserDao extends JpaRepository<User, Long> {

    /**
     * 自定义SQL
     */
    @Query("select u from User u where u.password = ?1")
    User findByPassword(String s);

    @Query("delete from User where password = ?1")
    @Modifying
    @Transactional
    void deleteByPassword(String s);
    @Query("select u from User u")
    List<User> queryUserList();
}
