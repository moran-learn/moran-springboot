package com.example.controller;

import com.example.result.Result;
import com.example.service.PushProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

/**
 * @Description
 * @Author Moran
 * @Date 2023-05-31 22:09
 */
@RestController
public class PushProcessController {
    private final Logger logger = LoggerFactory.getLogger(PushProcessController.class);
    @Autowired
    private PushProcessService pushProcessService;

    /**
     * 推送数据接口
     */
    @GetMapping("/push/process")
    public Result pushData() {
        try {
            pushProcessService.pushData();
        } catch (ExecutionException | InterruptedException e) {
            logger.error("推送异常",e);
        }
        return Result.ok();
    }

}
