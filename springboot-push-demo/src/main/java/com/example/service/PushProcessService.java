package com.example.service;

import java.util.concurrent.ExecutionException;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-03 11:41
 */
public interface PushProcessService {
    void pushData() throws ExecutionException, InterruptedException;
}
