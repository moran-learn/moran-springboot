package com.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(basePackages = {"com.example.mapper"})
public class SpringbootPushDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootPushDemoApplication.class, args);
    }

}
