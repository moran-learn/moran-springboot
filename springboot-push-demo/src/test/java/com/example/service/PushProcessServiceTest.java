package com.example.service;

import com.example.service.impl.PushProcessServiceImpl;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-03 13:41
 */
class PushProcessServiceTest {

    @Test
    void pushData() throws ExecutionException, InterruptedException {
        new PushProcessServiceImpl().pushData();
    }
}