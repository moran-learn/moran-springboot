package com.example.aspect;

import com.example.annotation.LogAnnotation;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 18:47
 */
@Component
@Aspect
public class LogAspect {

    private final static Logger log = LoggerFactory.getLogger(LogAspect.class);

    @Around("@annotation(logAnnotation)")
    public Object around(ProceedingJoinPoint point, LogAnnotation logAnnotation) throws Throwable {
        String className = point.getTarget().getClass().getName();
        String methodName = point.getSignature().getName();
        long startTime = System.currentTimeMillis();
        String ClassMethod = className + "." + methodName + "()";
        //获取注解方法
        MethodSignature signature = (MethodSignature) point.getSignature();
        LogAnnotation logDemo = signature.getMethod().getAnnotation(LogAnnotation.class);
        Object result = point.proceed();
        log.info(ClassMethod + " by called: " + logDemo.value() + ", total use :" + (System.currentTimeMillis() - startTime) + " ms !");
        return result;
    }
}
