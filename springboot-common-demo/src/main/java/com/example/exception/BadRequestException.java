package com.example.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-04 19:00
 */
public class BadRequestException extends RuntimeException {
    private Integer status = BAD_REQUEST.value();

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BadRequestException(String mgs) {
        super(mgs);
    }

    public BadRequestException(HttpStatus status, String mgs) {
        super(mgs);
        this.status = status.value();
    }
}
