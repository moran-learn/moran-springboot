package com.example.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @Description 异常工具类
 * @Author Moran
 * @Date 2023-06-04 19:13
 */
public class ThrowableUtil {
    /**
     * 获取堆栈信息
     */
    public static String getStackTrace(Throwable throwable){
        StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            throwable.printStackTrace(pw);
            return sw.toString();
        }
    }
}
