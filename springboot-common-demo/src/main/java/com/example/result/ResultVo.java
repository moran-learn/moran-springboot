package com.example.result;

import com.example.status.HttpStatus;

import java.util.HashMap;

/**
 * @Description 统一响应数据
 * @Author Moran
 * @Date 2023-06-04 18:54
 */
public class ResultVo extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;
    /**
     * 状态码
     */
    private static final String CODE = "code";
    /**
     * 返回内容
     */
    private static final String MSG = "msg";
    /**
     * 数据对象
     */
    private static final String DATA = "data";

    public ResultVo() {

    }

    /**
     * @param code 状态码
     * @param msg  返回内容
     */
    public ResultVo(int code, String msg) {
        super.put(CODE, code);
        super.put(MSG, msg);
    }

    public ResultVo(int code, String msg, Object data) {
        super.put(CODE, code);
        super.put(MSG, msg);
        super.put(DATA, data);
    }

    public int getCode(int code){
        return code;
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ResultVo success() {
        return ResultVo.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static ResultVo success(Object data) {
        return ResultVo.success("操作成功", data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static ResultVo success(String msg) {
        return ResultVo.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static ResultVo success(String msg, Object data) {
        return new ResultVo(HttpStatus.SUCCESS, msg, data);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ResultVo warn(String msg) {
        return ResultVo.warn(msg, null);
    }

    /**
     * 返回警告消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static ResultVo warn(String msg, Object data) {
        return new ResultVo(HttpStatus.WARN, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static ResultVo error() {
        return ResultVo.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 错误消息
     */
    public static ResultVo error(String msg) {
        return ResultVo.error(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 错误消息
     */
    public static ResultVo error(String msg, Object data) {
        return new ResultVo(HttpStatus.ERROR, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 错误消息
     */
    public static ResultVo error(int code, String msg) {
        return new ResultVo(code, msg, null);
    }

    /**
     * 方便链式调用
     *
     * @param key   键
     * @param value 值
     * @return 数据对象
     */
    @Override
    public ResultVo put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
