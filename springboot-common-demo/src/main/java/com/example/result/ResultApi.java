package com.example.result;

import com.example.exception.GlobalException;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @Description 异常实体类
 * @Author Moran
 * @Date 2023-06-04 19:10
 */
@Data
public class ResultApi {
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    private final static Logger log = LoggerFactory.getLogger(ResultApi.class);
    private static ResultApi ResultApi = new ResultApi();

    private Integer status;
    private String message;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime timestamp;

    public ResultApi() {
        this.timestamp = LocalDateTime.now();
    }

    /**
     单例模式获取对象
     */
    public static ResultApi getResultApi(){
        if (ResultApi == null){
            ResultApi = new ResultApi();
        }
        return ResultApi;
    }

    public static ResultApi error(String message){
        ResultApi result = getResultApi();
        result.setMessage(message);
        return result;
    }

    public static ResultApi error(String message, Integer status){
        ResultApi result = getResultApi();
        result.setMessage(message);
        result.setStatus(status);
        return result;
    }
}
