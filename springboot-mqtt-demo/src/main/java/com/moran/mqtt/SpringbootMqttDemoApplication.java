package com.moran.mqtt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMqttDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMqttDemoApplication.class, args);
    }

}
