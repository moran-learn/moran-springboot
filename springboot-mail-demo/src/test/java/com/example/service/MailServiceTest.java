package com.example.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-18 12:18
 */
@SpringBootTest
class MailServiceTest {

    @Autowired
    private MailService mailService;

    @Test
    void sendMail() {
        mailService.sendMail("wuningfei117@163.com", "测试邮件", "hello this test email");
    }
}