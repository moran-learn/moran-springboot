package com.example.controller;

import com.example.annotation.LogAnnotation;
import com.example.result.ResultVo;
import com.example.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author Moran
 * @Date 2023-06-18 12:35
 */
@RestController
@RequestMapping("mail/")
public class MailController {
    @Autowired
    private MailService mailService;

    @RequestMapping("sendMail")
    @LogAnnotation("发送普通邮件")
    public ResultVo sendMail() {
        return ResultVo.success("success", mailService.sendMail("wuningfei117@163.com", "测试邮件", "hello this test email"));
    }

    @LogAnnotation("发送HTML邮件")
    @RequestMapping("sendHTMLMail")
    public ResultVo sendHTMLMail() {
        String html = "<html>\n" +
                "<body>\n" +
                "<h2>this a html email</h2>\n" +
                "</body>\n" +
                "</html>";
        return ResultVo.success("success", mailService.sendHtmlMail("wuningfei117@163.com", "测试邮件", html));
    }

    @LogAnnotation("发送模板邮件")
    @RequestMapping("sendTemplateMail")
    public ResultVo sendTemplateMail(){
        int code = (int) (Math.random() * 9 + 1) * 100000;
        mailService.sendTemplateMail("wuningfei117@163.com", "这是模板邮件", String.valueOf(code));
        return ResultVo.success("操作成功");
    }
}
